/* eslint-disable unicorn/prefer-module */
module.exports = {
    parser: "@typescript-eslint/parser",
    extends: [
        "plugin:@typescript-eslint/recommended",
        "plugin:react/recommended",
        "plugin:react-hooks/recommended",
        "plugin:unicorn/recommended",
        // "plugin:prettier/recommended",
        // "plugin:prettier/react",
        // "plugin:prettier/@typescript-eslint",
    ],
    plugins: ["@typescript-eslint", "react", "prettier"],
    rules: {
        "react/react-in-jsx-scope": "off",
        "unicorn/prevent-abbreviations": "warn",
        "unicorn/prefer-node-protocol": "off",
        // "prettier/prettier": [
        //     "error",
        //     {},
        //     {
        //         usePrettierrc: true,
        //     },
        // ],
    },
    globals: {
        React: "writable",
    },
    env: {
        jest: true,
    },
};
