#!/bin/bash

echo "collecting stas for badges"

yarn outdated
if [ $? = 0 ];
then
    echo "{\"schemaVersion\":1, \"label\":\"dependencies\", \"color\":\"green\", \"message\":\"all up to date\"}" > badges.json
else
    echo "{\"schemaVersion\":1, \"label\":\"dependencies\", \"color\":\"red\", \"message\":\"some outdated\"}" > badges.json
fi