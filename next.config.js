module.exports = {
    future: {
        webpack5: true,
    },
    i18n: {
        locales: ["sk"],
        defaultLocale: "sk",
    },
    pageExtensions: ["js", "jsx", "ts", "tsx"],
    webpack: function (config) {
        config.module.rules.push({
            test: /\.md$/,
            use: "raw-loader",
        });
        return config;
    },
};
