import Head from "next/head";
import Link from "next/link";
import React from "react";
import Footer from "../sections/footer";
import PageHeader from "../sections/page-header";
import MainMenu from "../ui/main-menu";

/* eslint-disable @typescript-eslint/no-empty-interface */
export interface IIndexLayoutProperties {}

function IndexLayout({
    children,
}: React.PropsWithChildren<IIndexLayoutProperties>): JSX.Element {
    return (
        <>
            <Head>
                <meta charSet="utf-8" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta
                    name="description"
                    content="Rozcestník na vašej/našej ceste za duševným zdravím. Nájdete tu rôzne zdoje, na ktoré som v priebehu rokov narazil, prípadne mi pomohli."
                />
                <meta name="author" content="Jozef Mera" />
                <title>Križovatka duše</title>
            </Head>
            {/* <!-- Navigation--> */}
            <nav
                className="navbar navbar-expand-lg navbar-light fixed-top"
                id="mainNav"
            >
                <div className="container">
                    <Link href="/">
                        <a className="navbar-brand">Križovatka duše</a>
                    </Link>
                    <MainMenu />
                </div>
            </nav>
            {/* <!-- Page Header--> */}
            <PageHeader />
            {/* <!-- Main Content--> */}

            {children}

            <hr />
            {/* <!-- Footer--> */}
            <Footer />
        </>
    );
}

export default IndexLayout;
