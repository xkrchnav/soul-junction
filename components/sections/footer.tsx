function Footer(): JSX.Element {
    return (
        <footer>
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-md-10 mx-auto">
                        <ul className="list-inline text-center">
                            <li className="list-inline-item">
                                <a
                                    href="https://www.youtube.com/channel/UCab0t36kcxzV-i3I70ORKgQ"
                                    target="_blank" rel="noreferrer"
                                >
                                    <i className="icon-youtube"></i>
                                </a>
                            </li>
                            <li className="list-inline-item">
                                <a
                                    href="https://www.facebook.com/sdusevninemocivpartnerstvi"
                                    target="_blank" rel="noreferrer"
                                >
                                    <i className="icon-facebook"></i>
                                </a>
                            </li>
                            <li className="list-inline-item">
                                <a
                                    href="https://gitlab.com/xkrchnav/soul-junction"
                                    target="_blank" rel="noreferrer"
                                >
                                    <i className="icon-gitlab"></i>
                                </a>
                            </li>
                        </ul>
                        <p className="copyright text-muted">
                            Copyright &copy; Križovatka duše 2021
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;
