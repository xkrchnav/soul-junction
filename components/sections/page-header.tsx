import Search from "../ui/search";

export interface IPageHeaderProperties {
    title?: string;
    subHeading?: string;
}

function PageHeader(properties: IPageHeaderProperties): JSX.Element {
    return (
        <header
            className="masthead"
            style={{ backgroundImage: "url('assets/img/home-bg.jpg')" }}
        >
            <div className="overlay" />
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-md-10 mx-auto">
                        <div className="site-heading">
                            {!properties.title && !properties.subHeading && (
                                <>
                                    <h2>
                                        Keď som sa prvý krát stretol s
                                        psychiatrickou diagnózou, nebolo toľko
                                        dostupných zdrojov a jediná možnosť bola
                                        liečebňa. Preto som začal tvoriť tento
                                        &quot;rozcestník&quot;. Prajem, aby nám
                                        všetkým pomáhal na ceste za uzdravením,
                                        zotavením a celkovo lepším životom.
                                    </h2>
                                    <Search />
                                </>
                            )}
                            {(properties.title || properties.subHeading) && (
                                <>
                                    <h1>{properties.title}</h1>
                                    <span className="subheading">
                                        {properties.subHeading}
                                    </span>
                                </>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
}

export default PageHeader;
