import { useState } from "react";
import { Typeahead } from "react-bootstrap-typeahead";
import { useSearchContext } from "../../utils/search-context-provider";

function Search(): JSX.Element {
    const searchContext = useSearchContext();
    const [options, setOptions] = useState<string[]>([]);

    function onInputChange(input: string) {
        if (typeof searchContext?.miniSearch?.autoSuggest === "function") {
            setOptions(
                searchContext.miniSearch
                    .autoSuggest(input, { fuzzy: 0.5 })
                    .map((x) => x.suggestion) || []
            );
        }
    }

    return (
        <>
            <Typeahead
                minLength={2}
                id="main-search"
                placeholder="Čo práve hľadáte?"
                emptyLabel="Nič sme nenašli..."
                onInputChange={onInputChange}
                options={options}
                onChange={(o) =>
                    typeof searchContext?.onSearch === "function" &&
                    searchContext.onSearch(o[0])
                }
            />
            {/* <div className="input-group input-group-lg">
                <div className="input-group-prepend">
                    <button
                        className="input-group-text icon-search"
                        type="submit"
                    ></button>
                </div>
                <div className="input-group-append">
                <button
                className="btn btn-outline-secondary dropdown-toggle"
                type="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                >
                Všetky kategórie
                </button>
                <div className="dropdown-menu">
                <a className="dropdown-item" href="#">
                Audio
                </a>
                <a className="dropdown-item" href="#">
                Blogy
                </a>
                <a className="dropdown-item" href="#">
                Sociálne médiá
                </a>
                <a className="dropdown-item" href="#">
                Weby
                </a>
                <div role="separator" className="dropdown-divider"></div>
                <a className="dropdown-item" href="#">
                Podrobné hľadanie... (tagy)
                </a>
                </div>
            </div> 
            </div> */}
        </>
    );
}

export default Search;
