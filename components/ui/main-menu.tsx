import Link from "next/link";
import React from "react";
import { useDarkModeContext } from "../../utils/dark-mode-context-provider";

function MainMenu(): JSX.Element {
    const [isDarkMode, toggleDarkMode] = useDarkModeContext();

    return (
        <>
            <button
                className="navbar-toggler navbar-toggler-right"
                type="button"
                data-toggle="collapse"
                data-target="#navbarResponsive"
                aria-controls="navbarResponsive"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                Menu
                <i className="icon-menu"></i>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <Link href="/zello-dialogy">
                            <a className="nav-link">Zello dialógy</a>
                        </Link>
                    </li>
                    <li>
                        <div className="lightdark">
                            <input
                                type="checkbox"
                                className="checkbox"
                                id="chk"
                                checked={isDarkMode}
                                onChange={toggleDarkMode}
                            />
                            <label className="label" htmlFor="chk">
                                <i className="icon-nightlight_round"></i>
                                <i className="icon-brightness_low"></i>
                                <div className="ball"></div>
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
        </>
    );
}

export default MainMenu;
