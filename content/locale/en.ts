export const en: Record<string, Record<string, number | RegExp | string>> = {
    "/": {
        title: "Soul Junction",
        welcome: "Welcome to Soul Junction",
        intro:
            "Personal web which aim is to be resource of mental health related information, some list of links and place for creativity.",
        continueReading: "Continue...",
        home: "Home",
        wrap: "WRAP",
    },
    "/[slug]": {
        title: "Soul Junction",
    }
};
