export const sk: Record<string, Record<string, number | RegExp | string>> = {
    "/": {
        title: "Križovatka duše",
        welcome: "Vitajte na Križovatke duše",
        intro:
            "Osobná stránka, ktorej cieľom je stať sa zdrojom informácií o duševnom zdraví, rozcestník či miesto k tvorbe.",
        continueReading: "Pokračovať...",

        home: "Úvod",
        wrap: "OPZ",
    },
    "/[slug]": {
        title: "Križovatka duše",
    }
};
