---
title: Rozhovor s paní Motlovou
locale: sk
tags: rozhovor,schizofrénia
description: Schizofrenie je závažné onemocnění, které postihuje přibližně 1 procento populace. Příčinám schizofrenie stále dobře nerozumíme. R. Tamchyna tuto záludnou nemoc rozebírá s L. Bankovskou Motlovou z Psychiatrického centra Praha.
created: "2014-11-09"
url: https://prehravac.rozhlas.cz/audio/3185632
---

komentár: je to povídání o schizofrenii jako nemoci, jak se projevuje, jak se léčí, o rodinné edukaci, o stacionářích v ČR a hlavně mě zaujal konec o ITAREPsu. Nesouhlasím s tím, že paní doktorka prezentuje, že léky hrají nejdůležitější roli v léčbě...
