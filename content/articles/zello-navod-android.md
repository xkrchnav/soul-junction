---
title: Zello návod - Android
locale: sk
tags: zello dialógy,návody,android,mobil
description: Návod na použitie zella v Androide.
created: "2014-11-15"
---

# Zello návod - Android

## Video návod

### Inštalácia, základné ovládanie

Ako zello nainštalovať z obchodu google play, ako sa prihlásiť na kanál a začať prvé vysielanie:

[![youtube video](https://img.youtube.com/vi/_LCbmiqAoh8/0.jpg)](https://www.youtube.com/watch?v=_LCbmiqAoh8 "Prihlasenie na kanál a prvé vysielanie")

### Pokročilé nastavenie hlasitosti, kontakt "echo"

Ako si nastaviť hlasnejšie konkrétnych ľudí ak ostatných počujem dobre:

[![youtube video](https://img.youtube.com/vi/40N9surtzX8/0.jpg)](https://www.youtube.com/watch?v=40N9surtzX8 "Hlasitosť a kontakt echo")

### Prepínanie medzi kanálmi, solo mód, história

Ako prepínať medzi kanálmi a nestratiť históriu z iného kanálu:

[![youtube video](https://img.youtube.com/vi/fUpRJGyEeVY/0.jpg)](https://www.youtube.com/watch?v=fUpRJGyEeVY "prepínanie medzi kanálmi")
