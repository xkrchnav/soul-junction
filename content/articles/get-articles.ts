import fs from "fs";
import path from "path";
import { prepareMDX } from "../../utils/prepare-mdx";

const { readdir, readFile } = fs.promises;

export interface IArticle {
    slug: string;

    title: string;
    category: string;
    locale: string;
    tags?: string[];
    description: string;
    created: string;
    url: string;

    code: string;
}

interface ICategory {
    name: string;
    directory: string;
}

const categories: ICategory[] = [
    { name: "uncategorized", directory: "./content/articles" },
    { name: "audio", directory: "./content/articles/audio" },
    { name: "blogs", directory: "./content/articles/blogs" },
    { name: "social", directory: "./content/articles/social" },
    { name: "text", directory: "./content/articles/text" },
    { name: "web", directory: "./content/articles/web" },
];

async function getArticlesByDirectory(directory: string): Promise<IArticle[]> {
    const files = await readdir(directory);
    const result: IArticle[] = [];
    const category =
        categories.find((x) => x.directory === directory)?.name ||
        "uncategorized";
    for (const file of files) {
        if (file.slice(-3) === "mdx" || file.slice(-2) === "md") {
            const fileBuffer = await readFile(path.join(directory, file));
            const content = fileBuffer.toString().trim();
            const { code, frontmatter } = await prepareMDX(content);
            result.push({
                slug: file.split(".")[0],

                title: frontmatter.title,
                category,
                locale: frontmatter.locale,
                tags: frontmatter.tags?.split(",") ?? "",
                description: frontmatter.description,
                created: frontmatter.created,
                url: frontmatter.url ?? "",

                code,
            });
        }
    }
    return result;
}

async function getArticles(): Promise<IArticle[]> {
    const result: IArticle[] = [];
    for (const category of categories) {
        result.push(...(await getArticlesByDirectory(category.directory)));
    }

    return result;
}

export default getArticles;
