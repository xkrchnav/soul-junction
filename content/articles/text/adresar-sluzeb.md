﻿---
title: Adresář služeb
locale: sk
tags: krizová pomoc,poradenstvo,vzdelávanie,rehabilitácie
description: Adresář služeb pro lidi s duševním onemocnením v České republice
created: "2021-05-23"
url: http://www.adresar.vidacr.cz/
---

Čtenáři,
Před sebou máte sice malou, ale cennou elektronickou příručku. Obsahuje základní přehled poskytovaných tzv. psychosociálních služeb v České republice, najdete zde služby nejen zdravotní a sociální, ale i ty, které nejsou registrované. Tento elektronický adresář má potenciál stát se průvodcem každého z nás, kdo se dostane do situace, kdy budeme potřebovat pomoci zvládnout život s duševní nemocí – ať už v roli pacienta (rsp. uživatele), příbuzného, přítele či odborníka. Velice si cením solidního zpracování množství informací, které je snadno pochopitelné a zejména využitelné i pro čtenáře, kteří se v problematice psychických onemocnění orientují (zatím) jen málo.

To, že se jedná o menší elektronický adresář, není pouze důsledkem daru stručného vyjadřování se autorů, ale bohužel zejména faktu, že poptávka po službách pro osoby s duševní nemocí není ani zdaleka nasycena. Některé druhy služeb, jak je známe ze zahraničí, u nás neexistují, nebo se vyskytují ve vzácné podobě solitérů. Stále, byť snaha reformovat psychiatrickou a psychosociální péči směrem k individuálnímu přístupu k pacientům je již letitá, se nepodařilo v dostatečném množství, kvalitě a dostupnosti vybudovat a udržet potřebnou škálu zařízení a služeb.

Ráda bych si také posteskla, že žádný adresář nemůže být nikdy ve chvíli spuštění aktuální, protože od momentu uzávěrky vzniklo bezpochyby několik dalších služeb a jsou v adresáři zbytečně opomenuty či se služby změnily. Díky reformě psychiatrické péče snad i další služby vzniknou a budou ku prospěchu nás všech. Pokud najdete změnu služby nebo ji nenajdete vůbec, prosím, napište nám a bude tato informace zaktualizována či přidána.

Přeji nám všem proto, byť s určitou dávkou idealismu, aby v řadách zástupců exekutivní moci na všech myslitelných úrovních zvítězil zdravý rozum nad chaosem a populismem, aby se tedy český sociální a zdravotní systém dočkal kýžených reforem v praxi. Abychom my všichni, kteří někdy budeme potřebovat služby z oblasti péče o duševní zdraví, tuto péči dostali, ba abychom si mohli i vybrat. Pevně totiž věřím, že model kvalitních a dostupných sociálních služeb a psychiatrické péče se může stát realitou i v tomto státě.

S úctou a v obdivu všem těm, kdo se potýkají s psychickým onemocněním, ať už na kterémkoli z břehů
Bc. Andrea Studihradová
