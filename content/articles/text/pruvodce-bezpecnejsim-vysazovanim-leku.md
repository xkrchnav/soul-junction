﻿---
title: Průvodce bezpečnějším vysazováním psychiatrických léků
locale: sk
tags: ebook,will hall,vysadzovanie liekov
description: VIDA vydala tuto publikaci Willa Halla v roce 2013 za finanční podpory Úřadu vlády ČR.
created: "2014-08-18"
url: http://www.vidacr.cz/2013/06/08/pruvodce-bezpecnejsim-vysazovanim-psychiatrickych-leku/
---

Neexistuje jednotný návod, jak přestat užívat psychiatrické léky. Je ale, a tento průvodce to ukazuje, společná zkušenost, základní zjištění a významné informace, které mohou celý proces usnadnit. Mnoho lidí úspěšně přestalo užívat psychiatrické léky, a to s podporou nebo bez ní, zatímco pro jiné to bylo velice obtížné. Hodně lidí léky dále užívá, protože jejich přínos je pro ně větší než jejich zápory. Mnoho lidí však nadále užívá psychiatrické léky, aniž by uvážili a přezkoumali všechny možnosti, prostě proto, že nic jiného než léky neznají.
