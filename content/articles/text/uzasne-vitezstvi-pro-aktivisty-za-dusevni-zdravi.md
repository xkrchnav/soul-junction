﻿---
title: Úžasné vítězství pro aktivisty za duševní zdraví
locale: sk
tags: duševné zdravie,mad in america
description: Ředitel Národního ústavu duševního zdraví akceptuje myšlenky, které byly kdysi nahlíženy jako extrémní – Mad In America
created: "2015-02-02"
url: http://cvl.sweb.cz/dusevni_zdravi/Uzasne_vitezstvi.htm
---

# Bruce Levine

Původní článek: [http://www.madinamerica.com/2013/09/behind-amazing-victory-mental-health-activists-robert-whitaker/](http://www.madinamerica.com/2013/09/behind-amazing-victory-mental-health-activists-robert-whitaker/)

Pro aktivisty za reformu psychiatrie a pro Roberta Whitakera je to úžasné vítězství. 28. srpna 2013 oznámil ředitel Národního ústavu duševního zdraví (NIMH), Thomas Insel, že standardní léčba lidí, u kterých byla diagnostikována schizofrenie a jiné psychózy, potřebuje změnu.

Poté, co prozkoumal dvě dlouhodobé studie o schizofrenii a psychózách, došel Insel k závěru, který byl dříve považován za extrémní: Z dlouhodobého hlediska se některým jedincům s psychotickou zkušeností daří lépe bez medikace.

Insel konečně uznává, o čem aktivisté za reformu psychiatrie a investigativní reportér, Robert Whitaker, mluvili již léta – výzkum ukazuje, že standardní léčebné postupy americké psychiatrie ubližují mnoha lidem, kterým by pomohlo selektivnější a omezenější užívání léčiv a rozmanitější přístup, jako například ten používaný ve Finsku, který prokazuje nejlepší dlouhodobé výsledky ve vyspělém světě.

Tak jako mnoho aktivistů za reformu psychiatrie i Whitaker, Insel neodmítá úplně používání medikace, ale místo toho požaduje její uvážlivější používání. Insel dochází k závěru:

Vypadá to, že antipsychotika, která se jevila tak důležitá v časné fázi psychózy, z dlouhodobého hlediska zhoršují výhledy na úzdravu… Vypadá to, že co nyní nazýváme „schizofrenií“ může zahrnovat poruchy se zcela odlišnými dráhami. Pokud někteří lidé zůstanou dlouhodobě na medikaci, může to zabránit jejich úplnému uzdravení. Pro jiné může být přerušení medikamentózní léčby katastrofou.

Co se týče tohoto nedávného závěru ředitele Národního ústavu duševního zdraví, je podivuhodné, že znamená méně peněz pro farmaceutické firmy, které v minulosti těžce ovlivňovaly psychiatrickou léčbu pomocí svého finančního vlivu. Velké farmafirmy nesmírně profitovaly ze současných standardních léčebných postupů, které požadují celoživotní braní antipsychotik po jediné psychotické epizodě. Kvůli těmto léčebným postupům a rostoucímu používání antipsychotik při nepsychotických stavech, vydělávala antipsychotika ve Spojených státech v roce 2011 přes 18 miliard dolarů ročně. Antipsychotikum Abilify v prvním čtvrtletí roku 2013 vydělalo ze všech léků nejvíce a je na dobré cestě k hrubému výdělku 6 miliard dolarů v letošním roce (mezi korporacemi, které vydělaly loni jen přibližně 5 miliard dolarů, jsou Facebook a Yahoo).

## Jak došlo k tomuto vítězství aktivistů?

Po několik desetiletí sváděla skupinka aktivistů za reformu psychiatrie, skládající se z disidentských odborníků v oblasti péče o duševní zdraví a z „přeživších psychiatrickou péči“ [survivors] (kterým samým se dostalo neužitečné nebo kontraproduktivní léčby) namáhavý boj za opravdu informovaný výběr zahrnující větší možnost volby, která by odrážela různorodost populace se schizofrenií a jinými psychózami.

Whitaker a kol. napsali v roce 1998 sérii článků pro Boston Globe, které se umístily ve finále soutěže o Pulitzerovu cenu za službu veřejnosti. Čím podrobněji Whitaker pátral, tím více nacházel evidentních problémů standardní psychiatrické léčby. Vypátral dramatický vzrůst počtu lidí v USA v invalidních důchodech pro duševní onemocnění; výzkum odhalující selhání standardních postupů v psychiatrii; zjištění Světové zdravotnické organizace (WHO), že výsledky u schizofrenních onemocnění jsou mnohem lepší v Indii a Nigerii než ve Spojených státech; zjištění WHO o souvislosti mezi dobrými výsledky a dlouhodobým nezůstáváním na psychiatrických lécích; a možnosti volby léčení, které jsou mnohem efektivnější, než je americký standard psychiatrické péče. Jeho kniha Mad in America: Bad Science, Bad Medicine, and the Enduring Mistreatment of the Mentally Ill [Duševně chorým v Americe: Špatná věda, špatné léky a stálé špatné zacházení s duševně nemocnými] byla publikována v roce 2001. A Whitaker se dál věnoval tomuto příběhu, dokud psychiatrický establishment nebyl přinucen naslouchat.

V roce 2010 vyhrála kniha Anatomy of an Epidemic: Magic Bullets, Psychiatric Drugs, and the Astonishing Rise of Mental Illness in America [Anatomie jedné epidemie: Kouzelné střely, psychiatrické léky a ohromující vzestup duševních nemocí v Americe] Cenu investigativních reportérů a redaktorů za nejlepší investigativní novinařinu. Psychiatrický establishment Whitakera již nemohl dále ignorovat, a tak jej pozvali, aby promluvil v jejich institucích s nadějí, že ho zdiskreditují, ale to se jim nepodařilo.

## Výzkum, který přesvědčil ředitele Národního ústavu duševního zdraví

Jedna ze dvou hlavních výzkumných studií, kterou ředitel Národního ústavu duševního zdraví, Insel, citoval, aby odůvodnil svůj nedávný závěr, je studie o dlouhodobém léčení schizofrenie od Martina Harrowa, psychologa z lékařské fakulty na University of Illinois. Ironií osudu byla Harrowova studie uchráněna před zapomněním Whitakerem v Anatomy of an Epidemic – poté, co ji Národní ústav za duševní zdraví a psychiatrický establishment pohřbili. Jak poznamenává Whitaker:

V roce 2007, kdy [Harrow] publikoval své výsledky [za patnáct let práce] v Journal of Nervous and Mental Disease, vydal Národní ústav duševního zdraví osmdesát devět tiskových zpráv, mnoho o bezvýznamných věcech. Ale nevydal ani jednu o Harrowových zjištěních, přestože to byla nepochybně nejlepší studie o dlouhodobých výsledcích schizofrenních pacientů, která byla kdy uskutečněna ve Spojených státech.

Co je tedy tak důležité o Harrowově studii pro Whitakera – a nyní, konečně, pro Insela? V únoru 2012 Harrow publikoval výsledky svého dvacetiletého sledování, Do All Schizophrenia Patients Need Antipsychotic Treatment Continuously Throughout their Lifetime? A 20-Year Longitudinal Study [Potřebují všichni pacienti se schizofrenií léčbu antipsychotiky nepřetržitě po celý život? Dvacetiletá dlouhodobá studie], které znovu opakovaly jeho závěry z roku 2007. Harrow zjistil hlavně to, že pacienti diagnostikovaní se schizofrenií, kteří nebyli „na antipsychotikách po dlouhotrvajících obdobích, měli značně menší pravděpodobnost být psychotičtí a prožívali více období úzdravy.“

Harrow a jeho výzkumný tým zapojili pacienty ze dvou nemocnic v Chicagu, kteří byli diagnostikovaní se schizofrenií (a také pacienty diagnostikované s poruchami nálad s psychózou), aby zkoumali dlouhodobé výsledky. Všichni pacienti již dříve podstoupili konvenční medikamentózní léčbu během hospitalizace, a poté Harrow sledoval, jak se vyvíjely jejich životy, a pravidelně vyhodnocoval, jak se jim vede. Většina pacientů pokračovala v braní antipsychotik, zatímco asi třetina medikaci odmítla a přestala brát léky.

Výsledky za dvacet let, podobně jako ty za patnáct, ukázaly, že pacienti diagnostikovaní se schizofrenií (a pacienti diagnostikovaní s poruchami nálad s psychózou), kteří brali pravidelně antipsychotika, během těch 20 let ve skutečnosti prožili více psychózy, více úzkosti, měli horší kognitivní funkce a měli kratší období trvalejší úzdravy, než ti, kteří přestali brát antipsychotika.

Psychiatrický establishment, potom co nejprve Harrowovu studii pohřbil, se snažil, když na ni obrátil pozornost veřejnosti Whitaker v Anatomy of an Epidemic, její důležitost minimalizovat. Před Inslovým nedávným uznáním její důležitosti, tvrdil psychiatrický establishment, že Harrowova studie nic nedokazuje, protože jde o prospektivní, naturalistickou studii, nikoli o randomizovanou [znáhodněnou] koncepci. Ale v červenci 2013 publikoval holandský výzkumník Lex Wunderink se svým týmem studii v JAMA Psychiatry, která používala randomizovanou koncepci, „zlatý standard“ výzkumu – a Wunderink měl podobné výsledky jako Harrow. Psychiatrický establishment dostal mat.

Konkrétně podal Wunderink zprávu o sedmiletém sledování lidí diagnostikovaných se schizofrenií a příbuznými poruchami, kteří prožívali první psychotickou epizodu. Po šesti měsících remise [ústupu nemoci], která následovala po léčení antipsychotiky, bylo pacientům náhodně uloženo, aby buď dále brali antipsychotika, nebo aby je postupně vysazovali a přestali užívat. Sám Insel shrnuje výsledky: „Skupina, která vysadila léky, dosáhla během sedmi let dvojnásobné míry funkční úzdravy: 40,4 % oproti pouhým 17,6 % ve skupině, která pokračovala v braní léků.“

Wunderinkovu studii v JAMA Psychiatry doprovází komentář, který argumentuje, že psychiatrie musí odpovědět na tyto údaje a přijmout nové postupy při používání medikace:

Tím, že se posuneme k více personalizované a rozvrstvené medicíně, potřebujeme nejdříve identifikovat to velmi malé množství pacientů, kteří se nejspíše dokážou uzdravit z první psychotické epizody pouze díky intenzivní psychosociální intervenci. Pro všechny ostatní potřebujeme určit, které léky, po jakou dobu, v jaké minimální dávce a jaký rozsah intenzivních psychosociálních intervencí bude potřeba, aby se jim pomohlo k úzdravě, aby zůstali zdraví a vedli naplňující, užitečné životy. Tyto faktory byly zřídka cílem ve skutečném světě klinické psychiatrie – musíme se tím konečně zabývat, když jsme nyní vyzbrojeni pádnějšími důkazy k tomu, abychom čelili špatné praxi.

## Finský Otevřený Dialog: Lepší přístup již existuje

Zatímco Inselovo oznámení uznává zjištění jak Harrowa tak i Wunderinka, která se Whitaker dříve pracně snažil učinit známými, Insel opomíjí jednu osvědčenou alternativu. Terapie Otevřeným dialogem v severním Finsku již po více než dvě desetiletí poskytuje ten druh léčení, který Harrowův a Wunderinkův výzkum doporučuje. Whitaker podrobně líčí Otevřený dialog v Anatomy of an Epidemic a shrnuje to ve svém blogu ze 13. července 2013, „Harrow + Wunderink + Open Dialogue = An Evidence-based Mandate for A New Standard of Care“ [Harrow + Wunderink + Otevřený dialog = Mandát pro nový standard péče založený na prokázaných faktech].

Léčebný postup Otevřeného dialogu odkládá používání antipsychotik u pacientů s první psychotickou epizodou, místo toho využívá psychosociální podporu a selektivně používá úzkost snižující benzodiazepiny (např. Ativan, Klonopin, Valium) s nadějí, že pacienti se dokážou uvolnit a projdou první krizí, aniž by vůbec někdy užívali antipsychotika. A pokud pacienti potřebují užívat antipsychotika, postupy Otevřeného dialogu jim umožňují léky následně postupně vysadit.

Výsledky? „Selektivním užíváním antipsychotik,“ píše Whitaker, „Otevřený dialog osvědčuje ty nejlepší dlouhodobé výsledky ve vyspělém světě. 67 % pacientů s první epizodou nebylo po pět let vystaveno antipsychotikům a jen 20 % je udržováno pravidelně na lécích. 80 % pacientů s první epizodou se při takovýchto medikamentózních postupech dlouhodobě daří velmi dobře bez antipsychotik.“

Harrowova a Wunderinkova studie, Otevřený dialog a životy mnoha aktivistů z řad bývalých pacientů rozptylují mýtus, že se lidé plně neuzdraví z psychotických stavů, a pro mnoho těchto lidí, to že odmítli standartní psychiatrickou léčbu, byla jejich záchrana. Pro ně i pro nás ostatní je dobrou zprávou, že ředitel Národního ústavu duševního zdraví konečně uznal tuto skutečnost.

Bruce Levine, Ph.D., praktikující klinický psycholog, píše a mluví o tom, jak se společnost, kultura, politika a psychologie protínají. Jeho webová stránka je www.brucelevine.net .
