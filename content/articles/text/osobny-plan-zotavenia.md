---
title: Osobný plán zotavenia
locale: sk
tags: duševné zdravie
description: Osobný plán zotavenia (WRAP), ktorý je potrebné udržovať "živý". Založený v šuplíku nemá moc zmysel.
created: "2021-04-24"
url: https://schizofrenie.mistecko.cz/rubriky/plan-na-udrzeni-pohody
---

# Osobný plán zotavenia (WRAP)

Pôvodne som sa inšpiroval textom z tejto stránky, avšak za tie roky,
čo ho aktualizujem už sa tak nepodobá. Predsa však musím uviesť pôvodnú adresu, kde som inšpiráciu vzal :).

(_dátum poslednej aktualizácie_)

> (_citát, ktorý vystihuje toto obdobie_)

## Môj denný podporný plán

### "Kolečko"

Lianka mi vyrobila kolečko na chladničku (veľmi podobné tomu jej s lunou)

-   (0) Všetko ok
-   (1) Neusínat na vavřinech
-   (2) Nejvyšší čas "to" řešit
-   (3) Asi to nezvládneme - treba krizák, prípadne?

### Ako vyzerám, keď je "kolečko na OK (0)"?

(_sem napíšte ako sa cítite keď je všetko ok, chce to čas a pozorovať sa, prípadne spätnú väzbu od ostatných_)

### Čo robiť každý deň?

(_tieto denné veci ma udržujú v kondícii a pohode tak, aby "kolečko" zostalo na 0_)

### Čo potrebujem občas urobiť?

(_tieto občasné veci ma udržujú v kondícii a pohode tak, aby "kolečko" zostalo na 0_)

### Čo mi nerobí dobre?

(_čomu sa vyhýbať, aby sa koliesko nepretočilo k horšej fáze_)

## Spúšťače (čo spôsobuje točenie kolečka k horšiemu)

(_ja tu napríklad mám málo spánku, veľa práce, cezčasy,..._)

## Včasné varovné signály - kolečko (1-2)

(_podľa týchto signálov je na mne poznať, že sa niečo deje - môže to znamenať kolečko v 1 alebo 2_)

## Keď veci odštartujú zhoršenie, zhrútenie - kolečko (3)?

(_ak v týchto veciach budem pokračovať, tak to najskôr bez liečebne nezvládnem_)

## Akčný plán

### Kolečko je na (1)

(_ak je kolečko už v 1, treba zapojiťt tieto veci, resp. vynechať nejaké činnosti, aby sa situácia zas stabilizovala_)

### Kolečko je na (2)

(_ak je kolečko na 2 tak treba robiť všetko z 1 plus tieto veci, napríklad úplné vynechanie práce, atď._)

### Kolečko je na (3)

(_ak to nezvládame, koho najprv volať? sanitku? CDZ? krizák? Kamaráti?_)

## Podporujúci ľudia

### Moji pomocníci v kríze

(_na ktorých ľudí sa môžem obrátiť v dobe krízy?_)

## Koho veľmi nekontaktovať kým to neprejde?

(_koho naopak nekontaktovať vôbec v tomto stave?_)

## Lekárska starostlivosť + medikácia

### Psychiatrička, lieky

(_aké lieky aktuálne beriem, doktori, kontakty_)

### Psychoterapeut, peer

(_ďalšie možné pomáhajúce profesie_)

### Doterajšia história

(_vlastná história atak, hodí sa pri spätnom zamyslení sa_)

## Liečba

### Postupy pre redukciu symptómov

(_konkrétne postupy, bylinky, čokoľvek čo pomáha, relaxácia, vaňa, beh, krik,..._)

### Postupy ktoré chcem vynechať

(_ktoré postupy chcem úplne vynechať ak by som sa dostal do liečebne?_)

### Výber liečby

(_akú liečbu zvoliť - krizák, CZD? iné alternatívy? alebo mi vyhovuje liečebňa?_)

### Alternatívy k pobytu v nemocnici

(_čo skúsiť najskôr - liečebňa vždy počká - napríklad si urobiť takú domácu liečebňu a pod._)

## Pomoc ostatných

### Čo mi pomáha

(_čo pomáha od ostatných ľudí?_)

### Čo mi nepomáha

(_čo mi nepomáha? ak sa ľudia v blízkom okolí strachujú alebo nebodaj ľutujú napr._)

### Čo v prípade núdze

(_čo robiť v prípade nudze? skúsiť kamaráta, krízovú linku? ..._)

## Ak som v nebezpečí - kolečko je na 3?

### Opatrenia pre zníženie rizika straty peňazí

#### Banky

(_závisia na mne iní? nebezpečne rozdávam peniaze? treba to tu popísať a predať prístupy_)

#### Telefón

(_dnes je všetko cez telefón dostupné, banky atď. Takže všetko zablokovať, presne popísať čo hrozí. Sám tomu potom veriť_)

#### Obecné doporučenia

(_obecne čoho sa vyvarovať, nenakupovať, prácu voľno a podobne._)

## Zastavenie účinnosti plánu

(_na základe čoho poznáme, že už netreba ísť podľa plánu? môže byť prístup k mobilu, bankám, atď?_)

## Pokrizový plán

Ak by som zas všetko náhodou nezvládol… nelámať nad “sebou hůl”, ale skúsiť si zodpovedať nasledujúce otázky a vylepšiť plán…

### Co jsem se naučil o sobě a ostatních v průběhu krize?

### Je nějaká část mého Osobního plánu zotavení, která nefungovala tak, jak jsem doufal?

### Jaké změny mohu udělat ve svém OPZ, aby se dalo budoucí krizi předejít?
