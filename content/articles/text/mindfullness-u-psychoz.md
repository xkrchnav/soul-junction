﻿---
title: Mindfulness u psychóz? Už je to tady!
locale: sk
tags: mindfulness,psychóza,psychoterapia
description: všímavosť ako psychoterapia aj v liečbe psychóz
created: "2015-04-27"
url: https://www.psychoterapeut.net/downloads/benda2009.pdf
---

# MINDFULNESS U PSYCHÓZ? UŽ JE TO TADY!

## Chadwick, P. (2006). Person-Based Cognitive Therapy for Distressing

### Psychosis. Chichester: Wiley, 204 strany

### Jan Benda

S nástupem tzv. „třetí vlny“ kognitivních
a behaviorálních terapií se ve světě stále
častěji objevují pokusy využít klíčový prvek
těchto nových přístupů – všímavost
(angl. mindfulness) – také v léčbě psychóz.
Ve Spojených státech patří k průkopníkům
takových snah Brandon A. Gaudiano či
Louanne W. Davis, v Evropě jsou to např.
Antonio Pinto nebo Paul Chadwick. V knize
„Person-Based Cognitive Therapy for
Distressing Psychosis“ shrnuje posledně
jmenovaný autor vlastní terapeutické zkušenosti
z poslední dekády a podrobně popisuje
originální přístup zaměřený na redukci stresu
psychotických pacientů prostřednictvím
změny ve způsobu, jakým tito pacienti zacházejí
s vlastními symptomy, bludy, hlasy apod.
Kniha podle mého soudu stojí za pozornost.

První tři kapitoly knihy začleňují přístup
do širšího kontextu. Autor přibližuje počátky
PBCT, zdůrazňuje důležitost terapeutického
vztahu a obhajuje praxi tzv. „radikální spolupráce“,
tedy terapeutického vztahu prostého
jakýchkoli terapeutových předpokladů či
přesvědčení o tom, jak by se terapie měla
vyvíjet. Představuje dále Vygotského myšlenku
zóny nejbližšího vývoje a s ní spojené
čtyři zdroje stresu i duševní pohody: význam
symptomů, vztah k vnitřním zkušenostem,
schémata a symbolické já. Tyto čtyři oblasti
jsou podrobněji popsány v kapitolách 4–7,
které společně s 8. a 9. kapitolou přibližují
klinické využití přístupu.

V kapitole 4. autor nejprve zkoumá
možnost klienta poodstoupit od ustáleného
systému přesvědčení, sleduje, jak tato přesvědčení
vytvářejí stres a narušují chování,
a ukazuje, jak lze nalézat nový význam
symptomů, který podporuje duševní pohodu
(well-being). Proces, během něhož se klienti
učí přistupovat k nepříjemným hlasům,
myšlenkám a obrazům všímavě (mindfully),
je pak ilustrován v kapitole 5. Kapitoly 6
a 7 popisují cvičení atakující pocity studu,
zážitkové hraní rolí a metodu dvou židlí.
Objasňují také, jak podpořit nový „metakognitivní
vhled“ do povahy schematických
zkušeností já a jak jej využít jako základ
pro „metakognitivní sebepřijetí“ vyjádřené
v nově zformulovaném symbolickém já.
Závěrečné kapitoly (8 a 9) nabízejí pokyny
pro vedení skupinové terapie psychóz a zabývají
se procesem ukončování terapie.

Přístup Paula Chadwicka integruje kognitivní
teorii a terapii, všímavost (mindfulness),
rogeriánské principy a Vygotského
sociálně-vývojovou perspektivu procesu
změny. Nezaměřuje se na „normalizaci“ chování
psychotiků, nesnaží se vyvrátit jejich
bludy ani obhájit vlastní pojetí reality (ta-
MINDFULNESS U PSYCHÓZ? UŽ JE TO TADY!
Chadwick, P. (2006). Person-Based Cognitive Therapy for Distressing
Psychosis. Chichester: Wiley, 204 strany
Jan Benda
MINDFULNESS U PSYCHÓZ? UŽ JE TO TADY! 2009: 253–254
254
ková snaha by nejspíš jen zvětšila propast
mezi klientem a terapeutem). Namísto toho
se pokouší redukovat pocity úzkosti,
bezmoci, izolace a výlučnosti, které u klientů
vznikají jako sekundární reakce na nezvyklé
zkušenosti a symptomy. Můžeme
říci, že PBCT pomáhá klientovi rozvinout
všímavost a jejím prostřednictvím se zbavit
lpění na představách, myšlenkách, postojích
a reakcích, které prohlubují nepříjemné prožívání
(úzkost, atd.), a pomáhá dosáhnout
vhledu přinášejícího úlevu a nové možnosti,
jak zvládnout prožívání. V tomto ohledu se
podobá ostatním na všímavosti založeným
(mindfulness-based) přístupům včetně u nás
rozvíjené metodě satiterapie.

Chadwick bohužel nezná jiné způsoby rozvíjení
všímavosti než je možnostem klientů
přizpůsobená meditace nebo z ní odvozená
cvičení. To považuji za hlavní nedostatek
celého přístupu a zároveň oblast, kam může
být nasměrováno další bádání. Celkově jde
však podle mého soudu o metodu velmi slibnou
a nezbývá než doufat, že odolá tlakům
farmaceutických lobby, jejichž zájmem není
pomoci pacientům, ale zvýšit prodej léků,
a dokáže se prosadit v klinické praxi. Nová
kniha profesora Chadwicka představuje
v každém případě zdroj praktických informací
a inspirace pro profesionály pracující
s lidmi v psychóze i pro všechny zájemce
o využití všímavosti v psychoterapii.
