﻿---
title: Podpůrné mechanizmy při duševním onemocnění
locale: sk
tags: duševné zdravie,podpora,self-help
description: Václav Rolenec - Toto jsem sepsal pro svoje klienty, ale třeba se to bude hodit i někomu jinému. Klidně to šiřte dál, ať to pomůže co nejvíce lidem s duševním onemocněním.
created: "2015-02-02"
url: https://www.facebook.com/groups/Slysenihlasu/permalink/579736318828606/
---

Tento text vznikl částečným postahováním rad z internetu s menším přispěním informací a zkušeností peer konzultanta sdružení Práh. V tomto dokumentu je uvedeno několik podpůrných mechanizmů jak zlepšit své duševní zdraví, ale zároveň si neklade za cíl být jedinou univerzální pravdou. Některé zde uvedené rady Vám můžou pomoci, některé nikoliv, ale obecně by neměly uškodit. Pokud si u některých témat nebudete jisti, zkonzultujte je s odborníkem (Peer, psycholog, psychiatr).

## Smysl života

by měl být jedním z nejdůležitějších podpůrných mechanizmů. Duševně nemocný člověk se, ale bohužel svého smyslu života, který mněl ještě před onemocněním dost často vzdá a převezme za svou roli pacienta a postiženého. Je sice pravda, že je těžké se s duševním onemocněním vrátit do starých kolejí, ale je možné si najít nový směr a novou cestu nebo se i vrátit ke své staré cestě životem.

## Přátelství

s lidmi, kteří věří vaší schopnosti posílit sebe sama – může být zásadní. Ideálně by to měli být lidé, kteří vás zažili ve vašich „špatných dnech“, kteří jsou čestní a upřímní, myslí to s vámi dobře, jsou nablízku, když máte problémy, a jsou připraveni na obtíže, které mohou přijít. Současně by to měli být lidé, kteří znají meze, kam až mohou ve své pomoci jít, a jak „říci ne“, aby se vyvarovali stavu vyhoření. Důležité také je mít zdravé, tak i nemocné kamarády a střídat jejich návštěvy.

## Aktivity

jsou velice důležité při trávení volného času, dost často se stává, že duševně nemocní lidé se vzdají všech nebo většiny aktivit, které měli, když ještě byli zdraví. Proto je důležité se vrátit ke koníčkům a volnočasovým aktivitám, které měl člověk před onemocněním, pokud to jeho současný zdravotní stav dovolí. Stojí za zvážení si najít i nové koníčky a záliby, jelikož nemoc může člověka ovlivnit na tolik, že se k některým ze svých zálib vrátit nemůže. Aktivity jsou individuální a kolektivní a je třeba u nich najít rovnováhu v tom smyslu, že nelze dělat jen kolektivní aktivity, ale hlavně individuální aktivity zcela sám a stranit se společnosti lidí.

## Kreativita

je důležitá forma lidského vyjádření pocitů, emocí, životního postoje. Kreativitou lze vyventilovat své duševní problémy a lépe poznat sebe sama, například malováním, hudbou, modelováním, psaním atd. Když se člověk dostane na určitou úroveň zručnosti, tak se může jeho koníček stát i prací.

##Práce
může být jeden ze smyslů života a bývá dost často i potřebou člověka vytvářet nějaké hodnoty. Smysluplná práce sama o sobě nemusí nutně znamenat zaměstnání, i když je to dost často vnímané jako synonymum. Duševně nemocný člověk by měl práci vnímat jako určitou formu duševní rehabilitace, kdy řešení problému prohlubuje psychickou odolnost a fyzická zátěž může být jistou formou tělesného cvičení. Nezanedbatelnou devizou může být i finanční příjem a s ním ruku v ruce i lepší sociální situace a sebevědomí.

##Uvažte abstinenci
od rekreačních drog a alkoholu. Někteří lidé jsou vnímavější než ostatní, takže to, co ovlivní vaše přátele jen do jisté míry, může mít na vás daleko silnější vliv. Abstinence od drog a alkoholu upevní vaše duševní zdraví. Dokonce i mírnější drogy, jako třeba kofein nebo nikotin, mohou u někoho ohrozit zdraví, stabilitu a spánek. Cukr, včetně slazených džusů, a čokoláda mohou rovněž ovlivnit náladu. Někteří lidé mají reakce na zvýšení hladiny krevního cukru, nebo třeba na kofein, takovou, že může spustit psychózu. Buďte velice opatrní, pokud jde o marihuanu. Někomu může její užívání ulehčit symptomy nemoci, většině lidí však může způsobit deprese nebo psychotickou krizi.

##Odpočinek
zařiďte si zdravý spánkový režim. Léky na předpis, např. krátkodobě užívaný benzodiazepin, mohou být dobré pro případ nouze, ale začněte cvičením, bylinkovými přípravky, meduňkou, homeopatika, nebo doplňky jako melatonin, kalcium, magnesium. Pokud máte potíže se spánkem, vyhněte se stresujícím nebo konfliktním situacím, a nepožívejte nápoje obsahující kofein (káva, čaj a Cola). I když máte dost času, spánek před 23. hodinou je nejlepší. Dopřejte si čas oddechu od počítačů a vzrušení, než půjdete spát. Dopřejte si „šlofíka“, pokud to nenarušuje váš spánkový režim, a pokud vůbec nemůžete spát, zkuste jen zůstat v klidu vzhůru a odpočívat.

##Výživa
hraje velkou roli v duševní stabilitě a celkovém zdraví. Zjistěte si, na které potraviny či poživatiny jste alergičtí – například na gluten, kofein či mléko-laktózu. Uvažujte o potravinových doplňcích, jež mohou vyživit mozek a pomoci tělesnému zdraví, jako jsou vitamín C, rybí tuk, esenciální mastné kyseliny, vitamíny D a B, aminokyseliny (jako GABA, 5-HTP, tyrosin, theanin), a probiotika k uzdravení vašeho zažívacího traktu. Jezte hodně zeleniny, bílkovin, čerstvého ovoce, přirozeně sycených tuků; vystříhejte se balastních jídel (fast foody) – zkuste postupně nahrazovat jídlo, po kterém bažíte, zdravějšími potravinami. Buďte opatrní, pokud jde o vegetariánskou nebo veganskou dietu – může pomoci, ale může vás také oslabit a učinit „neuzemněnými“. Někdo je ovlivněn umělými sladidly, konzervanty a dalšími chemickými látkami v hotových jídlech. Zjistěte si něco o glykemickém indexu jídel, pokud máte nestabilní hladinu krevního cukru. Pokud užíváte bylinky nebo léky na fyzické potíže konzultujte se znalcem rostlinné léčby možné vzájemné působení, obzvláště jste-li v jiném stavu nebo kojíte.

### Podpůrné potraviny a poživatiny

mám s některými potravinami velice dobré zkušenosti při podpoře svého duševního zdraví. Jedná se o čistě subjektivní poznatky několika osob, kteří se těmito potravinami stravují a mají s nimi dobré zkušenosti. V podstatě je základním pravidlem při duševní nemoci vyživovat mozek kvalitními živinami, aby lépe fungoval.
**Skořice** je koření, které má velice blahodárný stimulační a nabuzující vliv při duševních onemocnění. Skořice prokrvuje orgány a snižuje hladinu krevního cukru. Pravidelné, případně i nárazové užívání trošku vyšších dávek skořice, maximálně dvě kávové lžičky denně například do kávy či čaje, případně s medem vám může udělat dobře. Je třeba pamatovat na to, že skořice je při větších množství hořká, tak je třeba si vyzkoušet dávkování. Například káva s trochou skořice je vynikající nápoj, ale když se to s ní přežene, tak je skoro nepoživatelná - hořká. Pro zajímavost, pravé kapučíno a některé další druhy kávy by se měly správně servírovat posypané skořicí.
**Citron** je přirozeně zásaditá potravina, i když chutná kysele. Je přirozeným zdrojem velkého množství vitamínu C a dalších důležitých výživových látek. Půlka až jeden citrón denně ráno spolu se snídaní do čaje má dobrý vliv na fyzické, tak i duševní zdraví.
**Česnek** je už podle našich babiček velice zdravá potravina. Má velmi dobré protizánětlivé účinky. Pravidelné požívání česneku podpoří vaše zdraví a tím pádem i duševní pohodu.
**Ovoce zelenina** jsou kapitola sama o sobě. Je potřeba je jíst ve větším množství protože jsou bohatým zdrojem vitamínů, minerálních látek, sacharidů, vlákniny.
**Maso** je také samostatná kapitola a docela kontroverzní. Je tu spíše zmíněno z toho důvodu, že moc nedoporučuji být při schizofrenii vegetariánem nebo přímo veganem. Maso je jednou ze základních potravin pro lidské tělo a pro by nemělo být vynecháno. Pokud už chcete být vegetariánem doporučuji jíst mořské ryby, vejce a mléko.
**Vlašské ořechy** a ořechy obecně jsou velice blahodárné na lepší fungování mozku. Několik ořechů každý den by vám mělo o něco zlepšit vaše duševní zdraví a pohodu.
**Zelený čaj** pokud rádi pijete čaj, zejména černý, tak stojí za zvážení začít pít zelený čaj. Jedná se totiž o skvělý přirozený detoxikant a zároveň stimulační a nabuzující nápoj, kterým můžete nahradit kávu.

##Cvičení
různé sporty, chůze, strečink, plavání nebo cyklistika dramaticky snižují úzkost a stres. Při sportu se do těla vyplavuje hormon Serotonin, který se nazývá „hormonem štěstí“ a má přímou závislost na hladinu dopaminu. Navíc sport pomáhá tělu při detoxikaci, hlavně potem a zrychlenou látkovou výměnou v orgánech při dlouhodobější zátěži. Také meditace v některých případech přispívá k uvolnění a odstranění stresu. Dbejte při ní vysokého pozoru, jak meditaci provádíte, jelikož při nesprávné meditaci může naopak dojít k zhoršení psychického stavu. Meditace provádějte pod dohledem nebo alespoň po poradě s odborníkem na dané téma.

##Pijte hodně čerstvé vody (bez příměsí)
po celý den: voda je základem pro detoxikační schopnost vašeho těla. Každá sklenka alkoholu, šálek kávy, čaje nebo soft drinků vás dehydruje a musí být vyrovnána stejným množstvím vody. Pokud vám nevyhovuje voda z kohoutku, pořiďte si filtr. Balená voda, zvláště pak z levnější cenové hladiny má buď stejné nebo i horší složení jak pitná voda z kohoutku a je cca 100x dražší. Když jste přehřátí, zpocení nebo dehydrovaní, doplňte sodík, cukr a elektrolyty draslíku.

##Chemické a toxické znečištění životního prostředí zatěžuje tělo a způsobuje fyzické a duševní problémy
někdy velmi vážné. Můžete-li, omezte svůj pobyt ve znečištěném prostředí, mezi znečišťujícími vlivy a látkami, jako jsou výpary čisticích prostředků na nábytek a koberce, úklidové prostředky, kyseliny, barviva, kysličník uhelnatý, venkovní znečištění, fluorescentní světla. Nechte si vyměnit amalgámové plomby za lepší. Někdy se stává, že psychicky nemocní lidé jsou citlivější na jakékoli toxiny.

##Prověřte si léky, které berete na fyzické potíže
při užívání kombinace více léků jak psychiatrických, tak ostatních se může stát, že dojde k dalším nežádoucím vedlejším účinkům. Psychiatři leckdy ani nevědí při větším koktejlu léků, k jakým může dojít dalším vedlejším nežádoucím účinkům.
