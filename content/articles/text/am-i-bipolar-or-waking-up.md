﻿---
title: Am I Bipolar or Waking Up?
locale: en
tags: ebook,bipolar
description: Zdarma ebook od Seana Blackwella
created: "2014-11-05"
url: https://media.wix.com/ugd/915faa_c24ad2bfb6509610122bdb028d25e927.pdf
---

#Hi Everyone!
In order to help spread the word regarding the healing
potential of bipolar disorder, this complete version of
“Am I Bipolar or Waking Up?” has been made available
free for everyone to share. However, for those of you who
prefer another format, or wish to support my work, both the
paperback and Kindle e-book version are available at
www.Amazon.com.
Enjoy the book!
Sean Blackwell

https://www.bipolarawakenings.com/
