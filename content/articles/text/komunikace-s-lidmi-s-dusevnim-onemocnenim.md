﻿---
title: Komunikace s lidmi s duševním oněmocnením
locale: sk
tags: mindfulness,psychóza,psychoterapia
description: preložený článok Communicating with people with mental illness
created: "2015-03-13"
url: https://www.facebook.com/download/666316076847703/Komunikace%20s%20lidmi%20s%20dusevnim%20onemocnenim.doc
---

Text prevzatý z facebookovej skupiny:

# Komunikace s lidmi s duševním onemocněním

John dorazil do Wasthingtonu ráno, po patnáctihodinové ceste autobusem. Utratil svých posledních dvacet dolarů za lístek a ted přemýšlí, jak sehnat víc cigaret, jak si tak pomalu kráčí ke Kapitolu. Rýsující se horizont připomíná něco z jiné doby nebo planety a táhne Johna kupředu, stejně jako hlasy v jeho hlavě, jenž zesilují. Rodiny procházející městem jen ztěží zaznamenají jeho potrhané oblečení a vystrašený výraz v jeho očích. Ti, kteří si toho všimnou, se mu opatrně klidí z cesty.

Jak se tak potuluje Pennsylvanskou Avenue, zahlédne bílou třpitící se budovu, domov prezidenta, a napadá ho, zda nejmocnější muž v zemi dokáže zastavit jeho bolest a utrpení. Šourá se směrem k bráně, ale ploty, betonové zátarasy a srážníci tajné služby zrazují Joha od dalšího přiblížení se. Pokračuje v ceste za svým cílem. Zastaví se, aby si prohlédl bronzového muže, stojícího na podstavci a upřeně hledícího na vodu, jako figurka na přídi velké vyklenuté mateřské lodi.
Sebere odvahu a vejde do kanceláře senátora.

„Musím vidět senátora!“ dožaduje se. „Máte domluvenou schůzku?“ ptá se zaměstnanec. „Ne, ale očekává mě,“ odpoví John sebejistě a pokračuje ve vysvětlování . „Musí je zastavit!“ „Koho?“ ptá se zaměstnanec. „ NSA nebo CIA , nevím jistě, kdo jsou. Kontrolují mé myšlenky pomocí Echelonu a V2K...vymývají mi mozek a ničí život, díky cyer červům v mém mozku!“

Tato scéna není neobvyklá ve vládních úřadech hlavního města a v jiných veřejných úřadech po celé zemi. Alespoň 10 procent populace vyhledá ošetření kvůli nějaké formě dušenvího onemocntění. Všichni známe lidi, kteří si prošli nejakou formou duševní poruchy nebo jste ji možná zažili na vlastní kůži.

V naší společnosti panuje silně negativní stigma spojené s duševním onemocněním, specialně s více závažnými formami jako je schizofrenie. Schizofrenie je typ psychozy, jenž je obecně charakterizován halucinacemi, roztříštěným myšlením a bludy. Schizofrenie je to, čím trpí John. Mnoho lidí se schizofrenií a jiní duševně nemocní nejsou o nic víc nebezbeční než veřejná populace, ale lidé se jich často bojí, díky jejich zvláštnímu a nepředvídatelnému chování. Samozřejmě někteří lidé s duševním onemocněním jsou nebezpeční. K tomu se vrátíme později.

Populární média podporují sterotypy o duševním onemocnení a nebezpečnosti, protože tak jsou většinou vykresleny. Noviny dělají senzaci z trestních činů, spáchaných lidmi s duševním onemocněním. Náš strach z lidí s duševním onemocněním pramení také z naší neschopnosti s nimi komunikovat a z nedostatečné znalosti o duševním onemocnění. Jen proto, že se chovají způsobem, který nám nedává smysl, není důvod, abychom jim neposkytovali stejnou službu, jako jiným voličům nebo hostům.

# Instrukce pro komunickaci s lidmi s duševním onemocněním.

Buďte zdvořilí. Pokud se někdo cítí respektovaný a vyslyšen, s velkou pravděpodobností respekt oplatí a zváží Váš názor.

Pokud někdo prochází stavem halucinací, mějte na paměti, že halucinace a bludy, které zažívá jsou jeho/její realitou a není možné mu/jí tuhle realitu vymluvit. Jejich halucinace a bludné myšlení jsou pro ně velmi opravdové a jsou jimi motivováni. Vyjádřete vaše pochopení pro stav, kterým prochází. Nepředstírejte, že tímto stavem procházíte též.

Mějte na paměti, že někteří lidé s paranoiou mohou být vystrašení a potřebují více tělesného prstoru než Vy.

Nepředpokládejte, že jsou hloupí a uvěří všemu, co jim řeknete.
Duševní onemocnění nemá nic společného se stupněm inteligence člověka. Nelžete, protože to většinou zničí vztah, který jste možná chtěli navázat.

Nepředávajte je „jako horkou bramboru“ dál, jen proto, že se jich chcete zbavit. Možná vám to na kratší dobu ušetří čas, ale může se vám to vrátit později, nebo způsobit problémy někomu jinému.
Kdokoliv, kdo je bezdůvodně posílán od člověka k člověku, se může naštvat a stát se agresivní.
Doporučte mu někoho jiného, jen v případě, že je to vhodné doporučení.

Zkuste pochopit, co chce dotyčný/ná vyjádřit. Často, pokud nevypnete své komunikační schopnosti, se vám podaří jej pochopit. Zjistěte, co pro něj můžete udělat.

Pokud je to zapotřebí, stanovte si stejné hranice, jako s ostatními lidmi. Například „Mám jen 5 minut na to, se vám věnovat.“ Nebo „Pokud budete křičet, nebudu schopný s vámi pokračovat v hovoru.“

Mějte uložený list ze zdroji komunitní péče, jako přístřeší, stravovací programy, služby péče o duševní zdraví, které jim můžete doporučit (pokud je potřebují). Někteří lidé návrh nepříjmou, ale jiní možná ano.

Zavolejte pomoc (policie, ostraha, kolegové) pokud se cítíe fyzicky ohroženi nebo potřebujete pomoc s uklidněním člověka.

Duševní onemocnění a násilí.

Duševní onemocnění samo o sobě nezvyšuje riziko násilí, pokud je ale duševní onemocnení kombinováno s dalšími rizikovými faktory jako je zneužívání návykových látek, toto riziko se zvyšuje. Předchozí výzkumy předložily smíšené výsledky, týkající se spojitosti mezi duševním onemocněním a násilím..

V roce 2009 v orientační studii řízenou Erikem Elbogenem a Sally Johnsonovou na Universitě v Severní Karolíně, byly vyhodnoceny data téměř 35 000 lidí. Se všemi byl dělán pohovor o jejich duševním zdraví, historii násilí a zneužívání návykových látek mezi lety 2001 a 2003. Shledali, že procento zůčastněných s duševním onemocněním odráželo procenta nalezená v obecné populaci a v jiných studiích.

V druhém pohovoru, řízeném v roce 2004 a 2005, účastníci byli dotazování na násilné chování, jako sexuální útoky, fyzické napadení nebo založení požáru v době mezi oběma pohovory. V době mezi pohovory 2.9 procent účastníků přiznalo své násilné chování. Když Elbogen a Johnsonová vyhodnotili možné asociace mezi duševním onemocněním, násilím a dalšími faktory, došli k závěru, že duševní onemocnění samo o sobě nepředurčuje násilí, ale problém zneužívání návzkových látek spolu s dušenvím onemocněním zvyšujde riziko násilí.

Když Elbogen an Johnsonová vyčlenili jen ty účastníky se závažným duševním onemocněním, 2.4 procent bylo agresivních. Pokud se ale dívali na ty účastníky s větší depresí a zároveň závislostí nebo zneužívání návykových látek, 6.47 procent bylo agresivních. Pokud se dívali na ty se schizofrenií 5.15 procent doznalo násilné chování v době mezi pohovory. Pokud člověk se schizofrenií také zneužíval návykové látky nebo měl problémy se závilslostí, násilné chování doznalo 12.66 procent v době mezi pohovory.
Nejvyšší riziko násilí bylo nalezeno u těch, kteří se potýkali s duševním onemocněním, problémem se zneužíváním návykových látek a měli záznam o násilí v minulosti. Tito účastníci měli 10x vyšší riziko násilného chování než ti, kteří se potýkali jen s duševním onemocněním.

Další faktory předurčující násilné chování

-   delikvence v nezletilosti nebo fyzické zneužívání.
-   být svědkem domácího násilí
-   nedávný rozvod
-   nezaměstnanost
-   viktimizace
-   mladší muži s nízkým příjmem

Ať už člověk s duševním onemocněním nebo bez něj, nikomu jen tak „nepřeskočí“ jak je často líčeno v médiích. Obecně je tu vývoj v chování vedoucímu k násilí a tohle chování je často zasnamenáno, jak se člověk k násilí přibližuje. Jako poskytovalé veřejných služeb, partneři, učitelé, přátelé, rodina, spolupracovníci bychom se měli naučit, jak tyto varovné známky v chování rozpoznat a svěřit se s našimi obavami lidem, kteří by mohli pomoci.Bohužel, může to být velmi obtížné, pomoci někomu, kdo naši pomoc nechce přijmout.

Pamatujte, že nejen lidé bez domova a chudí se potýkají s duševním onemocněním. Duševní onemocnění nemá racionální, ekonomické nebo intelektuální hranice. Možná pracujete v oboru jako sociální pracovník nebo psycholog. Možná jste v práci velmi zaneprázdněni trávávením času s lidmi s duševním onemocněním nebo bez něj. Mnoho lidí, které považujete za „divné“ mají právo vyhledat vaše služby a mají opravdovou potřebu, kterou vy můžete naplnit v souladu se svou pracovní náplní. Zkuste omezit vaši intervenci do relativně krátkých časových úseků, ale uvědomte si, že věnováním času efektivní komunikaci s člověkem vám možná ušetří mnoho času v
