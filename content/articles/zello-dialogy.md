---
title: Zello dialógy
locale: sk
tags: duševné zdravie,komunita,svojpomocná skupina,stretnutia,otvorený dialóg
description: Virtuálna svojpomocná podporná skupinka pre ľudí s duševnými ťažkosťami (nielen schizo...)
created: "2014-09-03"
---

# Zello dialógy

## O čo ide?

-   virtuálna svojpomocná podporná skupinka pre ľudí s duševnými ťažkosťami (nielen schizo...)
-   komunikácia je hlasová, budete potrebovať slúchatká a mikrofón, prípadne chytrý telefón
-   stretávame sa pravidelne raz za týždeň, cca na hodinku
-   viac informácií na fóre, kde sa skupinka vyvinula: [www.schizoforum.net](https://www.schizoforum.net/t264-topic)

## Priebeh stretnutia

1. Na začiatku sa pozdravíme, prípadne novým povieme ako to cca chodí.
2. Po pozdravení si povieme ako sa nám darilo od kedy sme sa počuli, čo nás teší či trápi.
3. Teraz nastáva príležitosť pre niekoho, čo má chuť niečo nazdieľať, prebrať, prediskutovať. Táto "fáza" má prednosť pred vlastnou témou zella.
4. Ak výjde čas, uvedieme tému a každý nazdieľa čo mu k tomu zrovna napadá, bez poradia, kto má čo chuť. Nemusí ani povedať nič...
5. Záverečné rozlúčenie. Novinka: často sa stáva, že zapálená diskusia prebieha aj po deviatej hodine. Nerád odchádzam bez rozlúčenia, ale zároveň viem, že ak preruším rozhovor pozdravom, stáva sa, že ostatní sa rozlúčia tak isto. Preto v tomto prípade môžeme využiť funkciu zella - pridať fotku (tá sa objaví všetkým prítomným v histórii bez toho aby rušila). Ak sa chcete teda rozlúčiť ale neprerušovať, odfoťte seba ako mávate, prípadne napíšte na papier rozlúčkový pozdrav :).

## Základné pravidlá

> Chcel by som, aby sa každý účastník cítil čo najlešpie, bezpečne, neohrozený odpočúvaním, vysmievaním sa alebo iným rizikom. Pravidlá nie sú nemenné a podľa vyvíjajúcich sa potrieb budú kryštalizovať.

1. sme na zellu, mali by sme ctiť pravidlá Zello komunity (žiaľ zatiaľ len v angličtine): http://zello.com/zello-community-guidelines.htm
2. prví účastníci boli členovia fóra (http://www.schizoforum.net/), bolo by preto vhodné, ak ste "zvonku", zaregistrovať sa na fóre a napísať pár príspevkov, nech sa trouchu "spoznáme", stačí napísať do tejto témy; ak sa pridá niekto mimo fórum, na začiatku skupinky budeme chcieť, aby sa novo prichádzajúci predstavil, uvidiedol svoje trápenie, situáciu, ako sa k tomuto dostal, podľa toho sa potom jednohlasne rozhodneme pokračovať alebo nie
3. kanál (Zello dialógy) je moderovaný (zatiaľ mnou a Tutom) je vždy vidieť, kto je prítomný; ak by sa vyskytol nejaký problém, je možné neprajníkov blokovať prípade zamknúť dočasne kanál heslom, všetko pre pocit bezpečia účastníkov
4. čo je povedané medzi členmi, zostáva medzi nimi
5. vlastná aktivita je dobrovoľná, ak účastník chce len počúvať, je to jeho vec a nikto ho do rozprávania nútiť nebude (s výnimkou nového účastníka viď bod 2.)
6. predstavte si, že sedíme v kruhu, v strede je kameň "slova", vždy hovorí ten, kto si ho vezme a ostatní počúvajú (v Zellu toto obrazné prevzatie kameňa bude stlačenie tlačidla vysielania)
7. v prípade, že konverzácia prebieha zapálene medzi dvoma členmi a vám napadne niečo, čím chcete prispieť, je vhodné skočiť do reči krátkym slovkom "brejk" (je to prevzaté z rádioamatérskeho žargónu). Tým diskutujúci budú mať možnosť dokončiť myšlienku a zároveň pustiť "brejka" ideálne predaním slova menovite; je tiež vhodné v zapálenej konverzácii nechať aspoň sekundu dve medzi odpoveďami, pre pustenie prípadného "brejka"

Ak by sa chcel niekto pridať, bez príspevku na fórum, budem rád, ak ma kontaktujete dopredu mailom (v zápätí stránky).

## Kedy?

Každú **stredu** a **sobotu** od 20:00 (SEČ).

## Technológia

Po prvom stretnutí sa zatiaľ program Zello osvedčil. Umožňuje totiž jednoduchú komunikáciu jeden-na-viacerých, je preložený do češtiny a slovenčiny a je dostupný pre všetky známe platformy [Windows](http://www.zello.com/data/ZelloSetup.exe), [Android](https://play.google.com/store/apps/details?id=com.loudtalks&referrer=utm_source%3Dzello.com%26utm_medium%3Dbanner%26utm_campaign%3Dmain), [iPhone](https://apps.apple.com/app/zello-walkie-talkie/id508231856?ls=1) a [Windows Phone](https://www.microsoft.com/cs-cz/p/zello/9wzdncrfj4pj?rtc=1&activetab=pivot:overviewtab).

Kanál "Zello dialógy" je dohľadateľný aj pomocou QR kódu:
[<img src="https://zello.com/channels/qr?name=Zello%20dial%C3%B3gy"/>](https://zello.com/channels/k/cBpyM)

Po stiahnutí a nainštalovaní v zariadení si zvoľte nick (prezývku). Potom si už stačí pridať kanál "Zello dialógy". Zistili sme, že Zello pre Windows je pomerne neintuitívne a preto prikladám podrobný návod ako nastaviť a následne hovoriť cez Zello na Windowse.

## Zello návody

-   [Windows PC](zello-navod-windows-pc)
-   [Android](zello-navod-android)

> Návody sú už staršieho dátumu, takže nemusia úplne sedieť s aktuálnou verziu :).

## Technická podpora

V prípade dotazov či technických problémov (a v prípade, že nie som na chate vyššie) ma môžete kontaktovať na mail jozef(zavináč)osvobodse(bodka)cz, prípadne (aj anonymne) do komentárov pod tento článok.
