---
title: PhDr. Světlana Soldánová - osobní příběh
locale: sk
tags: osobný príbeh,schizofrénia
description: Tyto stránky jsou věnovány mému osobnímu příběhu - mé cestě se schizofrenií, mým zkušenostem, objevům a všemu, co pomohlo a bylo užitečné.
created: "2015-05-18"
url: https://schizofrenie.mistecko.cz
---

Tyto stránky jsou věnovány mému osobnímu příběhu - mé cestě se schizofrenií, mým zkušenostem, objevům a všemu, co pomohlo a bylo užitečné.
