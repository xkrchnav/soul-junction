---
title: Jiří Bryan
locale: sk
tags: duševné zdravie,kultúra,denník,psychiatria
description: O čím dál tím všeho méně. A basta. Fidli. Li. Lije-li. Nelije-li, jdem ven.
created: "2014-10-16"
url: https://bryan.blog.respekt.cz
---

Vášnivý sběratel, příležitostný textař, mluvka, spisovatel, autoterapeut a písničkář. Syn, bratr, manžel, otec, strýc, dědeček. Je nás tu zkrátka víc. Než dost.
