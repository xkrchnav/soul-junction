---
title: Jindřiška Vlčková
locale: sk
tags: osobný príbeh,schizofrénia
description: Chtěla bych inspirovat všechny lidi, tedy i schizofreniky, kterým sama jsem, že všichni máme ostatním co říct. To je na nás právě to cenné, že si něčím projdeme a pak dokážeme život vyjádřit mnoha různými způsoby. Ostych není na místě, každý máme dar sdílet to, co je v nás. Každý jsme uvnitř krásný svým vlastním způsobem, i když zrovna trpíme nebo prožíváme bolest.
created: "2015-01-26"
url: https://jindriskavlckova.blog.idnes.cz
---

Chtěla bych inspirovat všechny lidi, tedy i schizofreniky, kterým sama jsem, že všichni máme ostatním co říct. To je na nás právě to cenné, že si něčím projdeme a pak dokážeme život vyjádřit mnoha různými způsoby. Ostych není na místě, každý máme dar sdílet to, co je v nás. Každý jsme uvnitř krásný svým vlastním způsobem, i když zrovna trpíme nebo prožíváme bolest.
