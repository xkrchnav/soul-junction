---
title: Renáta Holá
locale: sk
tags: duševné zdravie,drobnosti,práca,súkromné
description: Som normálnou matkou a manželkou, ktorá sa od roku 2000 potýka s vlastnými psychickými potiažami.
created: "2014-10-16"
url: https://dennikn.sk/autor/hola
---

Roky žijem s diagnózou psychickej poruchy. Píšem o tom, aké je to byť na psychiatrických liekoch, aké je to byť bez nich, o vlastných stavoch a postojoch.

### Starší blog:

[https://hola.blog.sme.sk](https://hola.blog.sme.sk)
