---
title: Martin Jarolímek
locale: sk
tags: psychiater
description: Blog kapacity Martina Jarolímka
created: "2015-01-26"
url: https://blog.aktualne.cz/blogy/martin-jarolimek.php
---

Psychiatr, narozen 1955 v Praze, vystudoval medicínu na 1. LF UK, zakladatel a hlavní lékař Denního psychoterapeutického sanatoria Ondřejov, kde se věnuje především léčbě schizofrenie. Hájí zájmy duševně nemocných a jejich rodin. Je zakladatelem a prezidentem České asociace pro psychické zdraví. Stál u zrodu dalších pacientských a rodičovských organizací (Sympathea, Fokus, Green Doors, Baobab) a iniciativ k transformaci systému psychiatrické péče v ČR.
