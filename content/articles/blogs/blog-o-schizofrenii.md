---
title: Blog o schizofrenii
locale: sk
tags: duševné zdravie,alternatívna liečba,novinky,blog
description: Blog o schizofrenii. Novinky klasické medicíny, alternativní léčba, rady, kultura a vše ostatní. Mějte svého psychiatra a berte léky.
created: "2014-08-27"
url: http://schizofreniecz.blogspot.cz
---

Autor blogu se již věnuje jiné práci, blog proto není aktualizován. Ti, kdo mají zájem si přečíst jeho články, a dozvědět se více z oblasti péče o duševní zdraví, mohou navštívit jeho nový projekt na adrese [www.produsevnizdravi.cz](https://www.produsevnizdravi.cz).
