---
title: Liga za duševné zdravie SR
locale: sk
tags: facebook,stránka,dobročinná organizácia
description: STOP PREDSUDKOM. Podporme akceptáciu ľudí s psychickými problémami v spoločnosti
created: "2014-08-26"
url: https://www.facebook.com/ligazadusevnezdravie
---

V roku 2000, v súvislosti s prípravou Roku duševného zdravia, sa objavila potreba vzniku silnej strešnej organizácie, ktorá bude nielen hlavným organizátorom kampane Roku duševného zdravia, ale bude aj po jej skončení pokračovať v činnosti najmä na poli zlepšenia podmienok a života duševne chorých, ako aj v oblasti prevencie duševných porúch. Liga je nepolitické, charitatívne, humanitné, neprofesionálne, nezávislé záujmové združenie občanov a právnických osôb a jej cieľom je aktívna podpora duševného zdravia.
Pri zakladaní Ligy za duševné zdravie spolupracovali Prof. Dr. Ivan Štúr, MUDr. Peter Breier, MUDr. Darina Sedláková, MUDr. Pavel Černák, Prof.PhDr. Anton Heretik, Doc. MUDr. Alojz Rakús, Ing. Dagmar Kardošová, Mag. art. Martin Knut. Predsedom Ligy sa stal Prof. Dr. I. Štúr.
Členmi ligy sú výbory viacerých organizácií Slovenská psychiatrická spoločnosť, Slovenská psychoterapeutická spoločnosť, Združenie pacientov Viktória, Celoslovenská asociácia pacientov a pacientských organizácií pre duševné zdravie Premeny, Združenie príbuzných a priateľov duševne chorých Opora a ODOS.
