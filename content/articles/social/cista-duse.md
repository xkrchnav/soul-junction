---
title: Čistá duše
locale: sk
tags: facebook,stránka,nezisková organizácia
description: Projekt o lidech, kteří onemocněli schizofrenií.
created: "2014-08-26"
url: https://www.facebook.com/cistaduse.cz
---

Čistá duše je projekt o lidech, kteří onemocněli schizofrenií. Chceme vám ukázat, že duševní onemocnění není žádné postižení, ale nemoc jako kterákoliv jiná.
