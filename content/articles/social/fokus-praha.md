---
title: Fokus Praha
locale: sk
tags: facebook,stránka,nezisková organizácia
description: I duše potřebuje péči a občas i doktora. Souhlasíte? Pak jste tu správně. Fokus Praha je nezisková organizace pečující o duševní zdraví.
created: "2014-08-26"
url: https://www.facebook.com/Fokus.Praha
---

Fokus vznikl jako jedna z prvních neziskových organizací již v roce 1990.
