---
title: Tým Dalet
locale: sk
tags: facebook,profil
description: Týmu koučů, terapeutů, supervizorů a lektorů přístupu zaměřeného na řešení.
created: "2014-11-05"
url: https://www.facebook.com/tym.dalet
---

Facebookový profil organizácie/týmu Dalet
