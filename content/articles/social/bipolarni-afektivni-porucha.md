---
title: Bipolární afektivní porucha - maniodeprese
locale: sk
tags: facebook,súkromná skupina
description: Skupina je safe space pro pacienty poruch bipolárního spektra a jejich bližních. Pokud jste jen zvědaví nebo hledáte informace o této nemoci, tohle není skupina pro vás.
created: "2014-11-09"
url: https://www.facebook.com/groups/364075876989118
---

-   Bipolární afektivní porucha (BAP, dříve označovaná jako psychoafektivní porucha nebo maniodepresivní psychóza či cyklofrenie) je psychická porucha, která se projevuje nadměrnými změnami nálad, vitality, psychických funkcí a někdy i omezuje schopnost celkového „přirozeného“ fungování.
-   Takto je nemoc popisována na nejrůznějších stránkách, ale to je pouhá teorie. Co však pro nás znamená v běžném denním životě? Jaké to je, žít s tímto "postrachem"? Pojďme si o tom říct a bojovat spolu, třeba to půjde aspoň trošku líp.

## PRO ZAJÍMAVOST:

-   Mnohé známé tvůrčí osobnosti trpěly manickou depresí a svou tvořivost spojovaly se svými proměnlivými stavy. Mnoho životů bylo ale také touto chorobou zruinováno.
-   BAP trpěl například český herec Miloš Kopecký, který na sklonku svého života dal souhlas se zveřejněním této skutečnosti. Podle jeho autentických slov přináší deprese větší utrpení než rakovina.
-   Chorobou trpěli i zpěváci Petr Muk, Richard Müller, Ozzy Osbourne a Kurt Cobain, spisovatelé Virginia Woolfová a Ota Pavel, skladatel Karel Svoboda, zpěvačka Demi Lovato či sérioví vrazi Ted Bundy a Robert Hansen, nebo také Pete Wentz z Fall Out Boy.

Touto nemocí také trpí herečka Catherine Zeta-Jones nebo zpěvačka Sinéad O'Connor
