---
title: Centrum pro rozvoj péče o duševní zdraví
locale: sk
tags: facebook,stránka
description: nestátní nezisková organizace
created: "2015-01-07"
url: https://www.facebook.com/crpdz/
---

Centrum pro rozvoj péče o duševní zdraví je nestátní nezisková organizace, jejímž posláním je zlepšovat péči o osoby s duševním onemocněním v České republice.
