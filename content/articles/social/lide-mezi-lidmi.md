---
title: Lidé mezi lidmi
locale: sk
tags: facebook,stránka
description: nezisková organizácia
created: "2014-08-30"
url: https://www.facebook.com/lidemezilidmi
---

Zdravotně-sociální portál Lidé mezi lidmi - informace pro lidi s handicapem a o dění kolem nich. Zvláštní důraz je kladen na duševní nemoci.
