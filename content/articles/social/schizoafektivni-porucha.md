---
title: Schizoafektivní porucha
locale: sk
tags: facebook,skupina
description: uzavretá facebooková skupina
created: "2015-03-13"
url: https://www.facebook.com/groups/663456033755777
---

Teraz už je skupina dokonca tajná, takže by ste museli požiadať o pozvánku niektorého z členov.
