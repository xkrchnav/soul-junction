---
title: Slyšení hlasů // Hearing Voices Network v ČR
locale: sk
tags: facebook,súkromná skupina
description: Skupina, která sdružuje lidi, kteří mají zkušenost se slyšením hlasů, paranoidními myšlenkami, vizemi a jinými neobvyklými zkušenostmi a jsou jim blízké principy Hnutí Slyšení hlasů - tzv. Hearing voices Movement.
created: "2014-11-05"
url: https://www.facebook.com/groups/Slysenihlasu
---

-   Skupina, která sdružuje lidi, kteří mají zkušenost se slyšením hlasů, paranoidními myšlenkami, vizemi a jinými neobvyklými zkušenostmi a jsou jim blízké principy Hnutí Slyšení hlasů - tzv. Hearing voices Movement.
-   Členové skupiny mohou sdílet informace, vzájemnou podporu, zkušenosti, zajímavé odkazy k tématu, či diskutovat.
-   Hnutí Slyšení hlasů nabízí alternativní přistup, ve kterém hlasy, nebo paraniodní myšlenky, nejsou automaticky nahlíženy jako patologie, ale nahlíží na ně jako na sice neobyčejnou, ale i přesto lidskou zkušenost, která může obsahovat smysl a význam, který může napomoci k integraci a celistvosti člověka.
    [http://slysenihlasu.cz](http://slysenihlasu.cz)
    [https://en.wikipedia.org/wiki/Hearing_Voices_Network](https://en.wikipedia.org/wiki/Hearing_Voices_Network)
