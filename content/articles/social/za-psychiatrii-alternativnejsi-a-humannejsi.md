---
title: Za psychiatrii alternativnější a humánnější
locale: sk
tags: facebook,súkromná skupina
description: Web pro postmoderní směry v tereapii a poradenství. Virtuální prostor pro diskuzi a výměnu informací pro všechny, kteří se zajímají o tyto směry nebo je praktikují.
created: "2014-08-18"
url: https://www.facebook.com/groups/123857190986755
---

## FOR ENGLISH SCROLL DOWN:-)

Web pro postmoderní směry v tereapii a poradenství. Virtuální prostor pro diskuzi a výměnu informací pro všechny, kteří se zajímají o tyto směry nebo je praktikují.

K pojmenování těchto stránek jsme použili slovo narativ, které tvoří kořen slov označující vyprávění či příběh v mnoha jazycích. Zdá se nám totiž, že dobře vystihuje přístupy, kterým je tento web věnovaný.

Takové, které podporují jak vyprávění a rozvíjení starých známých příběhů, tak i utváření příběhů nových, dosud neznámých.

Na těchto stránkách vás chceme informovat o dění v postmoderní terapeutické krajině (Aktuality), nabídnout vám mapu k jejímu prozkoumání, poskytovat informace, které vás seznámí s některými přístupy podrobněji, a v neposlední řadě vytvářet prostor pro diskuzi. Více o důvodech pro vznik webu najdete zde.

Registrujte se! Založte si svůj osobní či profesní profil a propojte se s dalšími lidmi, jejichž praxi ovlivnilo postmoderní hnutí! Můžeme začít společně utvářet jedinečnou podobu těchto směrů v české a slovenské krajině…

Website for postmodern approaches in therapy and counseling. Virtual space for discussion and information exchange for everyone who is interested in these approaches or who is practicing it.

To give this website a name, we chose “narativ” which is a root word for storytelling in many languages. We feel that it captures well those approaches which we want to address. Those, which promote both telling and elaborating on old known stories and constructing new, still unknown stories.

We want to inform you about what is happening in postmodern therapeutic landscape, to offer you map for exploring this landscape, to provide information which would tell you more about some of the approaches and, last but not least, to open a space for discussion.
We can start altogether to create a unique design of these approaches in Czech and Slovak countries…
