---
title: Dss - MOST
locale: sk
tags: facebook,stránka
description: nezisková organizácia poskytujúca sociálne a psychologické služby ľuďom s duševnou poruchou a ich príbuzným.
created: "2015-07-03"
url: https://www.facebook.com/DssMost
---

-   Dss - MOST, n.o. je nezisková organizácia poskytujúca sociálne a psychologické služby ľuďom s duševnou poruchou a ich príbuzným.
-   Poskytujeme psychologické a sociálne služby ľuďom, ktorí sa psychiatricky liečili a ich rodinám, rovnako ako ľuďom, ktorí čelia zvýšenej záťaži v rôznych formách.
-   Vieme, že počet ľudí, ktorí vyhľadávajú sociálne služby a psychologickú pomoc pre rôzne ťažkosti spojené s psychickou poruchou, je čoraz vyšší. Často im podpora pri ďalšom zvládaní problémov spojených s poruchou alebo ťažkosťami, ktoré práve prežívajú, výrazne uľahčuje život.
