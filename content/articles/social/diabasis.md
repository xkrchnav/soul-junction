---
title: Diabasis
locale: sk
tags: twitter,stránka,občianske združenia
description: PSYCHOSPIRITUÁLNÍ KRIZE, pomoc, vzdělávání, informace
created: "2021-05-02"
url: https://www.facebook.com/DIABASIS-103687293045966/
---

Pomáháme lidem, kteří zažívají mimořádné stavy vědomí, kdy se vynořování neobvyklých zkušeností může proměnit v potíže, které popisujeme termínem psychospirituální krize.
