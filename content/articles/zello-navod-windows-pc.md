---
title: Zello návod - Windows (PC)
locale: sk
tags: zello dialógy,návody,windows,pc
description: Návod na použitie zella v PC s OS Windows.
created: "2014-09-14"
---

# Zello návod - Windows (PC)

## Inštalácia

Najprv Zello stiahnite: [https://www.zello.com/data/ZelloSetup.exe](https://www.zello.com/data/ZelloSetup.exe)

Malo by stačiť kliknúť na odkaz, dať rovno spustiť alebo otvoriť. Ak to nejde, tak uložiť na disk (tak aby ste vedeli kam) - väčšinou to zostáva na spodku prehliadača a potom sa to dá spustiť ďalším kliknutím.

Po spustení stiahnutého súboru nainštalujte tradičkým spôsobom (klikaním ďalej ďalej ďalej a OK), na konci ešte možno padne otázka či chcete zello spustiť hneď. To môžete a rovno pokračujte obrázkovým alebo videonávodom nižšie.

## Video návod

Manželka po zhliadnutí obrázkového návodu zalapala po dychu, že to asi nie je úplne prehľadné pre nie technicky zdatného človeka. Preto som sa rozhodol urobiť aj video návod. Je trochu dlhší - 5 minút. Ale snáď bude dosť podrobný:

[![youtube video](https://img.youtube.com/vi/mrPgY4BXo1k/0.jpg)](https://www.youtube.com/watch?v=mrPgY4BXo1k "Návod pre windows")

### Pokročilý návod 1 - hlasitosť

Problémy s hlasitosťou - ako nastaviť individuálnu hlasitosť, svoju globálnu a podobne:

[![youtube video](https://img.youtube.com/vi/CJnwAIVCBq0/0.jpg)](https://www.youtube.com/watch?v=CJnwAIVCBq0 "Nastavenie hlasitosti")

### Pokročilý návod 2 - viac kanálov

Prepínanie medzi kanálmi, solo mód:

[![youtube video](https://img.youtube.com/vi/1XVb2ixvzA4/0.jpg)](https://www.youtube.com/watch?v=1XVb2ixvzA4 "Viac kanálov, sólo mód")

## Obrázkový návod

1. nastavenie češtiny (ak máte slovenský windows, tak sa asi na začiatku zapne slovenčina, tá však nie je v Zellu skoro podporovaná (čeština tiež nie je 100% preložená, ale je jej rozhodne viac)
   ![nastavenia](../images/zello0options.png)
   ![čeština](../images/zello1cestina.png)

2. pridanie kanálu "Zello dialógy"
   ![kanál](../images/zello2kanal.png)
   ![pridať kanál](../images/zello3pridatKanal.png)
   ![zoznam kanálov](../images/zello4zoznamKanalov.png)
   ![zello dialógy](../images/zello5zelloDialogy.png)
   ![dokonči](../images/zello6dokonci.png)

3. teraz už zostáva len vybrať kanál a "vysielať". Ak chcete hovoriť na kanále, vyberte ho myšou kliknutím na neho ľavým tlačítkom a potom vysielanie zapnete klávesou F7, prípadne kliknutím myšou na jedno z dvoch dolných tlačítok (na obrázku zelené a červené). To dlhšie zelené znamená, že kým ho držíte myšou, môžete hovoriť. To kratšie takzvane zamkne vysielanie nech ho nemusíte držať myšou celú dobu. Vypnete vysielanie zas kliknutím na zámeček.
   ![výber kanálu](../images/zello7vyberKanalu.png)
   ![vysielanie](../images/zello8vysielanie.png)

Červené vlnky na kanály znamená, že hovoríte vy, zelené zas že niekto iný na kanály. V popise Uživatelů online - vidíte koľko ľudí je na kanále. Echo - je testovací kanál, kde si môžete vyskúšať nastavenie hlasitosti mikrofónu a reprákov - to čo poviete on sám zopakuje. Taký test, ako v skype.
