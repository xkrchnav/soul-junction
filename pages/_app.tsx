import { AppProps } from "next/dist/next-server/lib/router/router";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { IntlProvider } from "react-intl";
import * as locales from "../content/locale";
import DarkModeContextProvider from "../utils/dark-mode-context-provider";

import "react-bootstrap-typeahead/css/Typeahead.scss";
import "../public/scss/clean-blog.scss";

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
    const router = useRouter();
    const { locale, defaultLocale, pathname } = router;

    const currentLocale = locale || defaultLocale || "sk";

    // TODO remove any! :)
    const localeCopy = (locales as any)[currentLocale];
    const messages = localeCopy[pathname];

    const [isDarkMode, setIsDarkMode] = useState(false);

    useEffect(() => {
        setIsDarkMode(
            window.matchMedia &&
                window.matchMedia("(prefers-color-scheme: dark)").matches
        );
    }, []);

    return (
        <IntlProvider
            locale={currentLocale}
            defaultLocale={defaultLocale}
            messages={messages}
        >
            <DarkModeContextProvider
                value={[
                    isDarkMode,
                    () => setIsDarkMode((previous) => !previous),
                ]}
            >
                <Component {...pageProps} />
            </DarkModeContextProvider>
        </IntlProvider>
    );
}

export default MyApp;
