import type { NextApiRequest, NextApiResponse } from "next";
import { Db, MongoClient } from "mongodb";
import { parse } from "url";

// Create cached connection variable
let cachedDatabase: Db;

// A function for connecting to MongoDB,
// taking a single parameter of the connection string
async function connectToDatabase(uri: string) {
    // If the database connection is cached,
    // use it instead of creating a new connection
    if (cachedDatabase) {
        return cachedDatabase;
    }

    // If no connection is cached, create a new one
    const client = await MongoClient.connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    // Select the database through the connection,
    // using the database path of the connection string
    const database = await client.db(parse(uri)?.pathname?.slice(1));

    // Cache the database connection and return the connection
    cachedDatabase = database;
    return database;
}

export default async (
    _: NextApiRequest,
    response: NextApiResponse
): Promise<void> => {
    const database = await connectToDatabase(process.env.MONGODB_URI!);
    const collection = await database.collection("comments");

    const comments = await collection.find().toArray();
    response.status(200).json({ comments });
};
