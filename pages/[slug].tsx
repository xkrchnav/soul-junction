import getArticles, { IArticle } from "../content/articles/get-articles";
import React from "react";
import { GetStaticPaths, GetStaticProps, GetStaticPropsContext } from "next";
import { MDX } from "../utils/mdx";
import ArticleLayout from "../components/layouts/article-layout";

export interface IArticleProperties {
    article?: IArticle;
}

function Article({ article }: IArticleProperties): JSX.Element {
    return (
        <>
            {article && (
                <ArticleLayout
                    title={article.title}
                    subheading={article.description}
                >
                    <article>
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-8 col-md-10 mx-auto">
                                    {article.url && (
                                        <a href={article.url}>{article.url}</a>
                                    )}
                                    <MDX source={article.code} />
                                </div>
                            </div>
                        </div>
                    </article>
                </ArticleLayout>
            )}
        </>
    );
}

export const getStaticPaths: GetStaticPaths = async () => {
    const articles = await getArticles();

    const paths = articles.map((a) => ({
        params: { slug: a.slug },
    }));

    return { paths, fallback: false };
};

// eslint-disable-next-line unicorn/prevent-abbreviations
export const getStaticProps: GetStaticProps<
    IArticleProperties,
    { slug: string }
> = async (context: GetStaticPropsContext<{ slug: string }>) => {
    const articles = await getArticles();

    const slug = context.params?.slug;

    return {
        props: {
            article: articles.find((x) => x.slug === slug),
        },
    };
};

export default Article;
