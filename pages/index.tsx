import React, { Fragment, useEffect, useRef, useState } from "react";
import { GetStaticProps, GetStaticPropsContext } from "next";
import Link from "next/link";
import getArticles, { IArticle } from "../content/articles/get-articles";
import { FormattedDate } from "react-intl";
import IndexLayout from "../components/layouts/index-layout";
import MiniSearch from "minisearch";
import SearchContextProvider from "../utils/search-context-provider";

export interface IHomeProperties {
    articles: IArticle[];
}

enum SortBy {
    Title,
    Date,
    Popularity,
}

export default function Home({ articles }: IHomeProperties): JSX.Element {
    const [count, setCount] = useState(5);
    const [filteredSlugs, setFilteredSlugs] = useState<string[]>();
    const [sortBy, setSortBy] = useState(SortBy.Date);

    const miniSearch = useRef<MiniSearch<IArticle>>(
        new MiniSearch<IArticle>({
            fields: ["title", "description"],
            idField: "slug",
        })
    );

    useEffect(() => {
        miniSearch.current.addAll(articles);
    }, [articles]);

    function onSearch(searchTerm: string): void {
        setCount(5);
        if (!searchTerm) {
            // eslint-disable-next-line unicorn/no-useless-undefined
            setFilteredSlugs(undefined);
        } else {
            const searchResult = miniSearch.current.search(searchTerm, {
                fuzzy: 0.2,
            });
            setFilteredSlugs(searchResult.map((x) => x.id));
        }
    }

    function getSortedFilteredArticles(): IArticle[] {
        articles.sort((a, b) => {
            switch (sortBy) {
                case SortBy.Title:
                    return a.title.localeCompare(b.title);
                case SortBy.Date:
                    return b.created.localeCompare(a.created);
                default:
                    return 0;
            }
        });

        return articles.filter(
            (x) => !filteredSlugs || filteredSlugs.includes(x.slug)
        );
    }

    return (
        <SearchContextProvider
            value={{ onSearch, miniSearch: miniSearch.current }}
        >
            <IndexLayout>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-md-10 mx-auto">
                            <div className="btn-group-sm btn-sort">
                                <button
                                    type="button"
                                    className="btn btn-secondary btn-sm dropdown-toggle"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                >
                                    Zoradiť
                                </button>
                                <div className="dropdown-menu">
                                    <button
                                        className="dropdown-item"
                                        onClick={() => setSortBy(SortBy.Date)}
                                    >
                                        Od najnovšieho
                                    </button>
                                    <button
                                        className="dropdown-item"
                                        onClick={() => setSortBy(SortBy.Title)}
                                    >
                                        Podľa abecedy
                                    </button>
                                </div>
                            </div>
                            {getSortedFilteredArticles()
                                .slice(0, count)
                                .map((article) => {
                                    return (
                                        <Fragment key={article.slug}>
                                            <div className="post-preview">
                                                <Link href={`/${article.slug}`}>
                                                    <a>
                                                        <h2 className="post-title">
                                                            {article.title}
                                                        </h2>
                                                        <h3 className="post-subtitle">
                                                            {
                                                                article.description
                                                            }
                                                        </h3>
                                                    </a>
                                                </Link>
                                                <p className="post-meta">
                                                    Štítky:&nbsp;
                                                    {article.tags?.map((t) => (
                                                        <Fragment key={t}>
                                                            <a>{t}</a>
                                                            {", "}
                                                        </Fragment>
                                                    ))}
                                                    dňa{" "}
                                                    <FormattedDate
                                                        value={article.created}
                                                    />
                                                </p>
                                            </div>
                                            <hr />
                                        </Fragment>
                                    );
                                })}
                            {count < getSortedFilteredArticles().length && (
                                <>
                                    {/* <!-- Pager--> */}
                                    <div className="clearfix">
                                        <a
                                            className="btn btn-primary float-right"
                                            onClick={() =>
                                                setCount(
                                                    (previous) => previous + 5
                                                )
                                            }
                                        >
                                            Zobraziť ďalšie...
                                        </a>
                                    </div>
                                </>
                            )}
                        </div>
                    </div>
                </div>
            </IndexLayout>
        </SearchContextProvider>
    );
}

// eslint-disable-next-line unicorn/prevent-abbreviations
export const getStaticProps: GetStaticProps = async ({}: GetStaticPropsContext) => {
    const articles = await getArticles();

    return {
        props: {
            articles,
        },
    };
};
