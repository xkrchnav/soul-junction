import React from "react";
import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
    render(): JSX.Element {
        return (
            <Html>
                <Head>
                    <link
                        rel="icon"
                        type="image/x-icon"
                        href="assets/favicon.ico"
                    />
                    {/* <!-- Google fonts--> */}
                    <link
                        href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic"
                        rel="stylesheet"
                        type="text/css"
                    />
                    <link
                        href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
                        rel="stylesheet"
                        type="text/css"
                    />
                    {/* <!-- Core theme CSS (includes Bootstrap)--> */}
                    <link href="css/styles.css" rel="stylesheet" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                    {/* <!-- Bootstrap core JS--> */}
                    <script src="https://code.jquery.com/jquery-3.5.1.min.js" />
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" />
                    {/* <!-- Core theme JS--> */}
                    <script src="js/scripts.js" />
                </body>
            </Html>
        );
    }
}

MyDocument.getInitialProps = async (context) => {
    // Render app and page and get the context of the page with collected side effects.
    const originalRenderPage = context.renderPage;

    context.renderPage = () => originalRenderPage({});

    const initialProperties = await Document.getInitialProps(context);

    return {
        ...initialProperties,
        // Styles fragment is rendered after the app and page rendering finish.
        styles: [
            <React.Fragment key="styles">
                {initialProperties.styles}
            </React.Fragment>,
        ],
    };
};

export default MyDocument;
