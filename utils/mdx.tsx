import React, { useMemo } from "react";
import { getMDXComponent } from "mdx-bundler/client";

export const MDX: React.FC<{ source: string }> = ({
    source,
}: {
    source: string;
}) => {
    const Component = useMemo(() => getMDXComponent(source), [source]);

    return <Component />;
};
