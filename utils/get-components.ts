import fs from "fs";
import path from "path";

const { readdir, readFile } = fs.promises;

interface Components {
    [file: string]: string;
}

export const getComponents = async (directory: string): Promise<Components> => {
    const components: Components = {};

    const files = await readdir(directory);

    for (const file of files) {
        if (file.slice(-3) === "tsx") {
            const fileBuffer = await readFile(path.join(directory, file));

            components[`./${file}`] = fileBuffer.toString().trim();
        }
    }

    return components;
};
