import { createContext, useContext } from "react";

export type DarkModeContextState = [
    isDarkMode: boolean,
    toggleMode: () => void
];

const DarkModeContext = createContext<DarkModeContextState>([
    false,
    () => {
        /**/
    },
]);

export function useDarkModeContext(): DarkModeContextState {
    return useContext(DarkModeContext);
}

export default DarkModeContext.Provider;
