import MiniSearch from "minisearch";
import { createContext, useContext } from "react";
import { IArticle } from "../content/articles/get-articles";

export interface ISearchContextState {
    onSearch?: (searchTerm: string) => void;
    miniSearch?: MiniSearch<IArticle>;
}

const SearchContext = createContext<ISearchContextState>({});

export function useSearchContext(): ISearchContextState {
    const context = useContext(SearchContext);

    if (!context || context.miniSearch === null || context.onSearch === null) {
        throw new Error("Search context was not properly initialized");
    }

    return context;
}

export default SearchContext.Provider;
