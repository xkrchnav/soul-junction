import { bundleMDX } from "mdx-bundler";
import path from "path";

process.env.ESBUILD_BINARY_PATH =
    process.platform === "win32"
        ? path.join(process.cwd(), "node_modules", "esbuild", "esbuild.exe")
        : path.join(process.cwd(), "node_modules", "esbuild", "bin", "esbuild");

export const prepareMDX = async (
    source: string,
    files?: Record<string, string>
): Promise<{ code: string; frontmatter: { [key: string]: string } }> => {
    const { code, frontmatter } = await bundleMDX(source, {
        files,
    });

    return { code, frontmatter };
};
