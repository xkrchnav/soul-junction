# Soul junction / Križovatka duše

This is my personal site with useful links for those who are finding some help with mental health. I share also my personal story in blogs, vlogs,...

I decided to build it from scratch (kinda custom CMS) on latest React technology stack as POC and improvements in my programming skills :).

## Requirements

- NodeJS 14+ [NodeJS homepage](https://nodejs.org/en/)
- yarn (`npm install -g yarn`)

## Usage

Use following to debug locally:
```
yarn
yarn dev
```

## Features

- Next.js (with yarn)
- TypeScript with all eslint, prettier checks
- CI setup
- dark mode detection

## Futures

- multilingual (Slovak, English) (react-intl)
- back-end supported (MongoDB)

## Domain

Current dev domain:
https://krizovatkaduse.eu
